using biblioteca_math;

namespace test_math;

public class UnitTest1
{
    [Theory]
    [InlineData(2, 3, 5)]//Prueba con datos fijos
    [InlineData(4, 3, 7)]//Prueba con datos fijos
    [InlineData(8, 11, 19)]//Prueba con datos fijos
    [InlineData(1, 1, 2)]//Prueba con datos fijos
    [InlineData(212, 35, 247)]//Prueba con datos fijos

    public void testSuma(int a, int b, int resultadoEsperado)
    {
        /// AAA
        /// Arange Act Assert

        /// Arange Prepara datos de entrada
        /// 
        int resutCalc;

        /// Act ejecuta
        resutCalc = Class1.suma(a, b);

        /// assert verifica 
        Assert.Equal(resultadoEsperado, resutCalc);

    }
    [Theory]
    [InlineData(2, 3, 6)]//Prueba con datos fijos
    [InlineData(4, 3, 12)]//Prueba con datos fijos
    [InlineData(8, 11, 88)]//Prueba con datos fijos
    [InlineData(1, 1, 1)]//Prueba con datos fijos
    [InlineData(212, 35, 7420)]//Prueba con datos fijos

    public void testMultiplicacion(int a, int b, int resultadoEsperado)
    {
        /// AAA
        /// Arange Act Assert

        /// Arange Prepara datos de entrada
        /// 
        int resutCalc;

        /// Act ejecuta
        resutCalc = Class1.multiplicacion(a, b);

        /// assert verifica 
        Assert.Equal(resultadoEsperado, resutCalc);

    }
}